import React from 'react'
import {Col, Row} from 'react-bootstrap';
import finger from "../images/finger.png";
import screen from "../images/screen.png";
import mobileapplication from "../images/mobileapplication.png";
import handGrab from "../images/handGrab.png";
import locked from "../images/locked.png";






function Features() {
  return (
    
    <div className="features">
        <div className='container pt-4 px-lg-5'>
       
            <div className='container px-lg-5'>
            <div className='container px-0 px-lg-5'>
                <Row className='pt-4 pt-md-3 pt-lg-4'>
                    <h2 className='h2size text-center'>Oqulo Features at a Glance</h2>
                    <p className='paragraphsize text-center'>Powerful functionalities that changes the way you do business.</p>
                </Row>
                <Row>
              
                    <Col className='pt-3 pt-md-0 pt-lg-1 pt-xl-5 order-1 col-12 col-md-4 col-lg-4'>
                        <Row className='pb-4 pb-md-0'>
                            <Col className='col-9 col-md-9 col-lg-9 order-2 order-md-1'>
                                <h6 className='h6size text-md-end'>Powerful Space Management</h6>
                                <p className='description text-md-end'>
                                    Management meeting room and desk bookings, create events, sell tickets, schedule private
                                    office showings, automate invoicing and connect with members --- all in one central dashboard.
                                </p>
                            </Col>
                            <Col className='col-3 col-md-3 col-lg-3 order-1 order-md-2'>
                                <img className='d-block mx-md-auto' src={finger} alt={''} />
                            </Col>
                        </Row>
                        <Row className='pb-4 pb-md-3'>
                            <Col className='col-9 col-md-9 order-2 order-md-1'>
                                <h6 className='h6size text-md-end'>Painless Integration</h6>
                                <p className='description text-md-end'>
                                    No matter what your website is built on. Oqulo is easy to setup and integrate with CRM and
                                    payment gateways. Go live in a matter of days.
                                </p>
                            </Col>
                            <Col className='col-3 col-md-3 justify-content-center order-1 order-md-2'>
                                <img className='d-block mx-md-auto' src={handGrab} alt={''} />
                            </Col>
                        </Row>
                    </Col>
                    <Col className='col-12 col-md-4 col-lg-4 px-md-0 order-3 order-md-2'>
                        <img className='screen d-block mx-auto' src={screen} alt={''} />
                    </Col>
                    <Col className='pt-md-0 pt-lg-1 pt-xl-5 col-12 col-md-4 col-lg-4 order-2 order-md-3'>
                        <Row className='pb-4 pb-md-1 pb-lg-0'>
                            <Col className='col-3 col-md-3'>
                                <img className='d-block mx-md-auto' src={mobileapplication} alt={''} />
                            </Col>
                            <Col className='col-9 col-md-9'>
                                <h6 className='h6size'>User-Friendly Interface</h6>
                                <p className='description'>
                                    Clients will find it easy to book and pay for their space, thanks to Oqulo's easy navigation
                                    and pixel-perfect design. Keep members up to date with Oqulo's community board and help desk
                                    features.
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='col-3 col-md-3 col-lg-3'>
                                <img className='d-block mx-md-auto' src={locked} alt={''} />
                            </Col>
                            <Col className='col-9 col-md-9 col-lg-9'>
                                <h6 className='h6size'>Secure Data & White Label Branding</h6>
                                <p className='description'>
                                    Get peace of mind in knowing that client information and sales data are
                                    stored in a secure server. Our white label service allows you to market
                                    this platform as your own.
                                </p>
                            </Col>
                        </Row>   
                    </Col>
             
                </Row>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Features