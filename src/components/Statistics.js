import React from 'react'
//import { Col, Row} from 'react-bootstrap';
import statistics from '../images/statistics.png';
import chart from '../images/chart.png';

function Statistics() {
  return (
    <>
    <br></br>
    <br></br>
    <br></br>
    <div className='container px-0 px-lg-5 statistics mt-sm-5 mt-md-3 mt-lg-5'>
        <div className='container px-0 px-lg-5 pt-md-1 pt-5 pt-xl-5'>
            <h2 className='h2size col-8 mx-auto col-md-12'>Stats Delivered Beautifully</h2>
            <p className='paragraphsize'>View sales charts, booking ratio and user behavior using Oqulo's 
               visual reporting feature.
            </p>
            <div className='px-md-5'>
                <img className='stats' src= {statistics} alt={statistics}/>
                <img className='chart' src= {chart} alt={''}/>
            </div>
        </div>
    </div>
    
    </>
  )
}

export default Statistics