import React from 'react';
import {Nav,Navbar,Container} from 'react-bootstrap';
import logo from '../images/logo.png'


function Header() {
  return (
    <div className='container pt-3 pb-2 mb-4 px-4 px-lg-5'>
       
        <div>
                <Navbar expand="lg px-0 px-md-5 px-lg-5">
                    <Container className='px-0 px-lg-5'>
                        <img src={logo} alt={''}/>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" className="custom-toggler navbar-dark" />
                       <Navbar.Collapse id="basic-navbar-nav nav">
                            <Nav className="ms-auto ">
                                <ul className="navbar-nav headerfont">
                                    <li className="nav-item">
                                        <a className="nav-link text-white" id="navbar" href="#discoveroqulo">DISCOVER OQULO</a>
                                    </li>	
                                    <li className="nav-item active">
                                        <a className="nav-link text-white mx-4" id="navbar" href="#features">FEATURES</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link text-white" id="navbar" href="#contact">CONTACT</a>
                                    </li>
                                    
                                </ul>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>

                </Navbar>
        </div>
    </div>
  )
}

export default Header