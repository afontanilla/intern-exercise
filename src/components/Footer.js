import React from 'react'
import Layer1 from '../images/Layer1.png';
import { Row, Col } from 'react-bootstrap';

function Footer() {
  return (
    <div className='col-12 com-md-12 footer align-items-center pt-md-5'>
        <div className='footer2 justify-content-center align-items-center pt-5'>
        <img className='mt-4 mb-4' src={Layer1} alt={''} />
            <Row className='justify-content-center col-11 col-md-7 mx-auto mb-3'>
                <Col className='col-11 col-md-auto'>
                    <p className='paragraphsize'>DISCOVER OQULO</p>
                </Col>
                <Col className='col-11 col-md-auto'>
                    <p className='paragraphsize'>FEATURES</p>
                </Col>
                <Col className='col-11 col-md-auto'>
                    <p className='paragraphsize'>CONTACTS</p>
                </Col>
             </Row>
             <div>
                 <p className='copywright pb-5'>Copywright © Oqulo 2018. All Rights Reserved.</p>
             </div>
        </div>
    </div>
  )
}

export default Footer