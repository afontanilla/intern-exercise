import React from 'react';
//import { Container } from 'react-bootstrap';
import { Col, Row } from 'react-bootstrap';
import iPhone7og from '../images/iPhone7og.png';


import Header from './Header.js';

function Banner() {
  return (
    
    <div className= "banner">
        <Header />
        <div className='container px-lg-5'>
            <div className='container px-lg-5'>
            <Row className='mx-md-5'>
                <Col className='phoneParent d-flex justify-content-center col-12 col-md-6 col-lg-5 offset-lg-1 order-2 order-md-1 '> 
                    <img className='phone' src={iPhone7og} alt={''} />
                </Col>
                <Col className ='paragraphs mt-2 col-12 col-md-6 order-1 order-md-2'>
                    <h2 className='h2size'>The Only Platform You'll Need to Run Smart Coworking Spaces & Serviced Offices</h2>
                    <p className='paragraphsizeBanner pt-3 pt-md-0'>Oqulo is built to sell, manage and grow your commercial real estate business. Collect payments, manage clients and run reports using our booking app. Engage members using our community messaging feature.
                    </p>
                    <p className='paragraphsizeBanner'>Be the first in line to take Oqulo for a test drive!</p>
                    <Row>
                        <Col className='email col-11 col-md-8 mb-1'>
                            <input className="input bannerInput" type="email" id="email" name="email" placeholder="     Email address" size="30"/>
                        </Col>
                        <Col className='col-6 col-md-4 pt-2 pt-md-0'>
                           <button className='button bannerButton' >NOTIFY ME</button>
                        </Col>
                        <p className='disclaimer mt-2 mt-md-1'>*No spam, That's a promise.</p>
                    </Row>                   
                </Col>
            </Row>
            </div>
        </div>

    </div>
   
  )
}

export default Banner