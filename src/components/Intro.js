import React from 'react'
//import {Row, Col} from 'react-bootstrap';
import image from "../images/image.png";



function Intro() {
    
    
  
    return (

    
    <div container-fluid className='intro mb-5'>
         <div className='container px-lg-5 pt-3 pt-md-5 pr-0 pl-0 mr-md-5 ml-md-5'>
            <div className='container px-0 mx-0 px-lg-5 px-md-5 mr-md-5 ml-md-5 '>
            <div className='container px-0 px-md-0 px-lg-5'>   
                    <h2 className='h2size col-11 col-md-5 pt-5 float-md-start'>Tried & Tested Space Management Software</h2>
                    <img className='introImage pt-4 col-12 col-md-6 pb-4 float-md-end' src ={image} alt ={image} />
                    <p className='paragraphsize col-11 col-md-5'>
                        Oqulo is a homegrown app that's been tested by real-life
                        businesses. Whether you operate on a single building or in 
                        multiple locations, Oqulo is designed to make your space leasing
                        operations hassle-free.
                    </p>
                    <p className='paragraphsize col-11 col-md-5'>
                        Your clients will have a smooth booking & online payment experience
                        and your concierge staff will be able to view occupancy stats and generate
                        reports at a click of a button.
                    </p>
            </div> 
            </div>     
         </div>
    </div>
  )
}            
              
                 
export default Intro