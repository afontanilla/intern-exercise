import React from 'react';
import {Row,Col} from 'react-bootstrap';

function CalltoAction() {
  return (
    <div className='calltoAction '>
       
        <Row className='calltoAction2 align-items-center  justify-content-center p-3'>
            <div>
              <h2>Launching Soon</h2>             
              <p className='col-12 mx-auto'>Sign up to get updates on Oqulo's public release</p>
              <Row className='justify-content-center'>
                  <Col className='email col-12  col-md-3 mb-3'>
                    <input className="input ctaInput" type="email" id="email" name="email" placeholder="     Email address" size="30"/>
                  </Col>
                  <Col className='col-8 col-md-2'>
                      <button className='button ctaButton' >TRY THE BETA</button>
                  </Col>
              </Row>
            </div>
       
          
        </Row>
    </div>
  )
}

export default CalltoAction