import './App.css';

/*import Header from "./components/Header";*/
import Banner from "./components/Banner";
import Intro from "./components/Intro";
import Features from "./components/Features";
import Statistics from "./components/Statistics";
import CalltoAction from "./components/CalltoAction";
import Footer from "./components/Footer";




function App() {
  return (
    <div className="App">
      
        <Banner />
        <Intro />
        <Features />
        <Statistics />
        <CalltoAction />
        <Footer />
      
      
    </div>
  );
}

export default App;
